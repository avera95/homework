import csv
import math
import sys
from importlib.machinery import SourceFileLoader
d = {}  # dictionary for expressions
t = {}  # the main dictionary
h = []
s = []
flag = 0
num_col = []  # the number of columns in each row
num_col.insert(0, 0)

if len(sys.argv) == 4:
    a = SourceFileLoader(str(sys.argv[3]), sys.argv[3]).load_module()
    flag = 1

with open(sys.argv[1]) as f:
    reader = csv.reader(f)
    i = 0  # the number of rows
    for row in reader:
        i = i + 1
        j = "A"
        num_col.insert(i, 0)
        for x in row:
            num_col.insert(i, num_col[i]+1)
            y = j+str(i)
            if x[:1] != '=':
                try:
                    if x.find('.') > 0:
                        t[y] = float(x)
                    else:
                        t[y] = int(x)
                except ValueError:
                    t[y] = x
            else:
                d[y] = x[1:]
            j = chr(ord(j)+1)

t.update(math.__dict__)

if flag == 1:
    t.update(a.__dict__)

for el in d:
    t[el] = eval(d[el], t)

with open(sys.argv[2], 'w') as f2:
    writer = csv.writer(f2)
    for j1 in range(1, i+1, 1):
        for i1 in range(1, num_col[j1]+1, 1):
            cur = chr(ord('A')+i1-1) + str(j1)

            if cur in t:
                s.append(str(t[cur]))
            else:
                s.append('ERROR')
        writer.writerow(s)
        s.clear()