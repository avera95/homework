#include <stdio.h>
#include <string.h>
#include <Python.h>
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

static int cur_var(lua_State *L, PyObject *val) {
    if (val == Py_None)
        lua_pushnil(L);

    else if (PyLong_Check(val) || PyFloat_Check(val)) {
        double v = PyLong_Check(val)? PyLong_AsDouble(val): PyFloat_AsDouble(val);
        lua_pushnumber(L, v);
    } 

    else if (PyUnicode_Check(val))
        lua_pushstring(L, PyUnicode_AsUTF8(val));

    else
        return -1;

    return 0;
}

static void new_dict(lua_State *L,PyObject *dict) {
    if (dict == Py_None)
        return;

    PyObject *term, *val;
    Py_ssize_t i = 0;

    while (PyDict_Next(dict, &i, &term, &val)) {
        PyObject *val2;

        if (!PyUnicode_Check(term))
            continue;

        lua_getglobal(L, PyUnicode_AsUTF8(term));
        
        if (lua_isboolean(L, -1))
            val2 = PyBool_FromLong(lua_toboolean(L, -1));

        else if (lua_isnumber(L, -1))
            val2 = PyFloat_FromDouble(lua_tonumber(L, -1));

        else if (lua_isstring(L, -1))
            val2 = PyUnicode_FromString(lua_tostring(L, -1));

        else {
            lua_pop(L, 1);
            continue;
        }

        PyDict_SetItem(dict, term, val2);
        Py_DECREF(val2);
        lua_pop(L, 1);
    }
}

static void add_var(lua_State *L, PyObject *dict) {
    PyObject *term, *value;
    Py_ssize_t i = 0;

    while (PyDict_Next(dict, &i, &term, &value)) {
        if (!PyUnicode_Check(term) && cur_var(L, value))
            continue;

        lua_setglobal(L, PyUnicode_AsUTF8(term));
    }
}

static PyObject * lua_eval(PyObject *self, const char *p, PyObject *globals, PyObject *locals) {

    lua_State *L = luaL_newstate();
    luaL_openlibs(L);

    add_var(L, globals);

    if (locals != Py_None)
        add_var(L, locals);

    if (luaL_loadstring(L, p)) {
        PyErr_SetString(PyExc_ValueError, lua_tostring(L, -1));
        lua_close(L);
        return NULL;
    }
        
    if (lua_pcall(L, 0, 1, 0) == 0) {

        PyObject *result;

        new_dict(L,globals);
        new_dict(L,locals);

        if (lua_isnil(L, -1)) {
            lua_close(L);
            Py_RETURN_NONE;
        } 

        else if (lua_isboolean(L, -1))
            result = PyBool_FromLong(lua_toboolean(L, -1));

        else if (lua_isnumber(L, -1))
            result = PyFloat_FromDouble(lua_tonumber(L, -1));

        else if (lua_isstring(L, -1))
            result = PyUnicode_FromString(lua_tostring(L, -1));

        else {
            PyErr_SetString(PyExc_ValueError, "Wrong type of result");
            lua_close(L);
            return NULL;
        }

        lua_close(L);
        return result;
    } 
    else {
        PyErr_SetString(PyExc_ValueError, lua_tostring(L, -1));
        lua_close(L);
        return NULL;
    }
}

static PyObject *eval(PyObject *self, PyObject *args, PyObject *args2) {

    PyObject *globals = Py_None;
    PyObject *locals = Py_None;

    const char *expr = NULL;
    static char *par[] = {"expr", "globals", "locals", NULL};

    if (!PyArg_ParseTupleAndKeywords(args, args2, "s|OO", par, &expr, &globals, &locals)) {
        return NULL;
    }

    int len = strlen(expr);

    char *p = NULL;
    const char* r = "return ";

    if (strchr(expr, '\n') == NULL) {
        p = (char *)calloc(len+strlen(r)+1, 1);
        strcpy(p, r);
        strcat(p, expr);
    }
    else {
        p = (char *)calloc(len+1, 1);
        strcpy(p, expr);
    }

    if (globals == Py_None)
        globals = PyEval_GetGlobals();

    PyObject *result = lua_eval(self, p, globals, locals);
    free(p);

    return result;
}

static PyMethodDef ModMethods[] = {
        {"eval", (PyCFunction)eval, METH_VARARGS|METH_KEYWORDS, "Running Lua code"},
        {NULL, NULL, 0, NULL}
    };

static struct PyModuleDef ModDef = {
    PyModuleDef_HEAD_INIT,
    "lua",
    "Working with Lua code",
    -1, ModMethods,
    NULL, NULL, NULL, NULL
};

PyMODINIT_FUNC PyInit_lua(void) {
    return PyModule_Create(&ModDef );
}