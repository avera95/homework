-module(atm).

-export([widthdraw/2]).

widthdraw(0, Banknotes, ok) ->
  {ok, [], Banknotes};

widthdraw(Amount, [], ok) ->
  {request_another_amount, [], []};

widthdraw(Amount, Banknotes, ok) ->
  [CurrentB|Others] = Banknotes,
  {CRes, OthersB} =
    if

      CurrentB > Amount ->
        Sum = 0,
        {[], [CurrentB]};

      true ->
        Sum = CurrentB,
        {[CurrentB], []}

    end,

  {Status, Result, OthersBanknotes} = widthdraw(Amount - Sum, Others, ok),

  if

    Status == ok ->
      {ok, lists:append(CRes, Result), lists:append(OthersB, OthersBanknotes)};

    Status == request_another_amount -> 
      {request_another_amount, [], Banknotes}

  end.

widthdraw(Amount, []) when Amount > 0 ->
  throw(no_cash);

widthdraw(Amount, Banknotes) ->
  widthdraw(Amount, lists:reverse(lists:sort(Banknotes)), ok).